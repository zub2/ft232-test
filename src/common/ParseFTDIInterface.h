/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of ft232-test.
 *
 * ft232-test is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ft232-test is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ft232-test.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FTDI_TEST_PARSE_FTDI_INTERFACE_H
#define FTDI_TEST_PARSE_FTDI_INTERFACE_H

#include <cstring>
#include <stdexcept>

#include <ftdi.h>

ftdi_interface parseFTDIInterface(const char * i)
{
		if (strlen(i) == 1)
		{
				switch (*i)
				{
				case 'A':
				case 'a':
						return INTERFACE_A;

				case 'B':
				case 'b':
						return INTERFACE_B;

				case 'C':
				case 'c':
						return INTERFACE_C;

				case 'D':
				case 'd':
						return INTERFACE_D;
				}
		}

		throw std::runtime_error("Wrong interface argument. Use one of A, B, C or D.");
}

#endif // FTDI_TEST_PARSE_FTDI_INTERFACE_H
