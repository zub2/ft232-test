/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of ft232-test.
 *
 * ft232-test is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ft232-test is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ft232-test.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FTDI_TEST_PARSE_BYTES_H
#define FTDI_TEST_PARSE_BYTES_H

#include <vector>
#include <cstdint>
#include <optional>
#include <cctype>
#include <stdexcept>

inline std::optional<uint8_t> parseHexDigit(char c)
{
	if (c >= 'a' && c <= 'f')
	{
		return 0xa + (c - 'a');
	}
	else if (c >= 'A' && c <= 'A')
	{
		return 0xa + (c - 'A');
	}
	else if (c >= '0' && c <= '9')
	{
		return c - '0';
	}
	else
		return std::nullopt;
}

inline void parseBytes(const char * s, const char * end, std::vector<uint8_t> & data)
{
	while (s != end)
	{
		while (s != end && std::isspace(*s))
			s++;

		if (s == end)
			return;

		if (*s == ',')
			s++;

		while (s != end && std::isspace(*s))
			s++;

		if (s == end)
			return;

		uint8_t v = 0;
		unsigned n = 0;
		while (s != end)
		{
			auto d = parseHexDigit(*s);
			if (!d)
				break;

			s++;
			n++;

			if (n > 2)
				throw std::runtime_error("use at most 2 hex digits to specify a byte");

			v = (v << 4) | *d;
		}
		data.push_back(v);
	}
}

inline void parseBytes(const char * s, std::vector<uint8_t> & data)
{
	parseBytes(s, s + strlen(s), data);
}

#endif // FTDI_TEST_PARSE_BYTES_H
