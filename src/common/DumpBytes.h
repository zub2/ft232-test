/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of ft232-test.
 *
 * ft232-test is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ft232-test is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ft232-test.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FTDI_TEST_DUMP_BYTES_H
#define FTDI_TEST_DUMP_BYTES_H

#include <cstdint>
#include <ostream>
#include <iomanip>
#include <vector>

inline void dumpBytes(std::ostream & os, const uint8_t * begin, const uint8_t * end)
{
	os << std::showbase << std::hex;
	bool first = true;
	for (const uint8_t * p = begin; p != end; p++)
	{
		if (first)
			first = false;
		else
			os << ", ";
		os << static_cast<unsigned>(*p);
	}
}

inline void dumpBytes(std::ostream & os, const std::vector<uint8_t> v)
{
	dumpBytes(os, v.data(), v.data() + v.size());
}

#endif // FTDI_TEST_DUMP_BYTES_H
