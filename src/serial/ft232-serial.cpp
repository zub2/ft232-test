/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of ft232-test.
 *
 * ft232-test is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ft232-test is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ft232-test.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This example uses libftdi to do UART communication with CBus bitbanging.
 */

#include <iostream>
#include <memory>
#include <stdexcept>
#include <sstream>
#include <string>
#include <limits>
#include <bitset>
#include <cstring>
#include <cstdlib>

#include <unistd.h>

#include <ftdi.h>

#include "../common/FTDIError.h"
#include "../common/FTDIContext.h"
#include "../common/ParseFTDIInterface.h"

namespace
{

const char * PROGRAM_NAME = "ft232-serial";

class FTDISerial
{
public:
	explicit FTDISerial(const char * device, ftdi_interface iface, int baudrate):
		m_context(device, iface)
	{
		// libftdi doc is not particularly helpful in explaining the bitmask but it is
		// probably ignored for BITMODE_RESET. With some luck ftdi_set_bitmode is an
		// equivalent of FT_SetBitMode from D2XX library. Some more info about
		// that function can be found in here:
		// https://www.ftdichip.com/Support/Documents/ProgramGuides/D2XX_Programmer's_Guide(FT_000071).pdf
		if (ftdi_set_bitmode(m_context, 0, BITMODE_RESET) < 0)
			throw FTDIError(m_context, "can't set bitmode");

		if (ftdi_set_baudrate(m_context, baudrate) < 0)
			throw FTDIError(m_context, "can't set baudrate");
	}

	int getActualBaudRate() const
	{
		return m_context.get()->baudrate;
	}

	FTDISerial & operator<<(char c)
	{
		write(&c, 1);
		return *this;
	}

	FTDISerial & operator<<(const std::string & s)
	{
		write(s.data(), s.size());
		return *this;
	}

	FTDISerial & operator<<(const char * s)
	{
		write(s, static_cast<int>(strlen(s)));
		return *this;
	}

	void write(const char * s, int length)
	{
		if (ftdi_write_data(m_context,
			const_cast<unsigned char*>(reinterpret_cast<const unsigned char*>(s)),
			static_cast<int>(length)) < 0)
			throw FTDIError(m_context, "can't write data");
	}

	void setCBus(std::bitset<4> outDirections, std::bitset<4> outValues)
	{
		const unsigned long cbusVal = (outDirections.to_ulong() << 4) | outValues.to_ulong();

		// It seems that calling ftdi_set_bitmode(..., BITMODE_CBUS) does not change the mode,
		// it just sets the CBus GPIO lines (directions and values) see:
		// https://github.com/legege/libftdi/blob/master/examples/bitbang_cbus.c
		// and also FT_SetBitmode description in
		// https://www.ftdichip.com/Support/Documents/ProgramGuides/D2XX_Programmer's_Guide(FT_000071).pdf
		if (ftdi_set_bitmode(m_context, static_cast<unsigned char>(cbusVal), BITMODE_CBUS) < 0)
			throw FTDIError(m_context, "can't set CBus GPIO values");
	}

private:
	FTDIContext m_context;
};

void printHelp()
{
	std::cout << "Usage: " << PROGRAM_NAME << " [-b baudrate] [-i interface] [-B] [-h] device_name\n"
			"\n"
			"A test program using libftdi to send example text via serial mode and optionally fiddle ACBUS GPIO lines.\n"
			"\n"
			"-b baudrate\tSets baud rate. Defaults to 9600 when option not used.\n"
			"-i interface\tRequests specific FTDI interface. Must be one of A, B, C or D. "
					"When not specified, ANY is passed to libftdi.\n"
			"-B\tEnable CBus bitbanging. When specified, sets all 4 CBus GPIO lines (ACBUS 5, 6, 8 and 9) "
					"to output and iterates over all combinations (0000b-1111b). The ACBUS GPIO lines need to "
					"be enabled in the chips' EEPROM first. With blank or no EEPROM nothing happens.\n"
			"-h\tPrints this help.\n"
			"\n"
			"The device_name is passed to libftdi's ftdi_usb_open_string. You can use "
			"e.g. 'i:vendor:product' or 'i:vendor:product:index', for example 'i:0x0403:0x6014:1'\n";
}

}

int main(int argc, char ** argv)
{
	if (argc > 0)
		PROGRAM_NAME = argv[0];

	const char * device = nullptr;
	bool doCBusBitbang = false;
	int baudRate = 9600;
	ftdi_interface iface = INTERFACE_ANY;

	int c;
	while ((c = getopt(argc, argv, "b:i:Bh")) != -1)
	{
		switch (c)
		{
		case 'b':
			baudRate = std::stoi(optarg);
			break;

		case 'i':
			iface = parseFTDIInterface(optarg);
			break;

		case 'B':
			doCBusBitbang = true;
			break;

		case 'h':
			printHelp();
			return EXIT_SUCCESS;

		default:
			std::cerr << "unknown option\n";
			return EXIT_FAILURE;
		}
	}

	if (optind == argc)
	{
		std::cerr << "Missing required argument: libftdi device name\n";
		return EXIT_FAILURE;
	}
	device = argv[optind];

	if (argc > optind + 1)
	{
		std::cerr << "Too many arguments.\n";
		return EXIT_FAILURE;
	}

	FTDISerial serial(device, iface, baudRate);

	std::cout << "Opened '" << device << "' in serial (RS232) mode\n";

	std::cout << "Actual baud rate: " << serial.getActualBaudRate() << '\n' << std::endl;
	if (doCBusBitbang)
		std::cout << "Doing CBus bit-banging.\n" << std::endl;

	// use all 4 CBus lines as output
	const std::bitset<4> outDirections(0b1111);

	unsigned i = 0;
	unsigned cbusValue = 0;
	std::stringstream s;

	while (true)
	{
		std::cout.put('.');
		std::cout.flush();

		s.str(std::string());
		s << "Test " << i++ << "\r\n";

		serial << s.str();

		if (doCBusBitbang)
			serial.setCBus(outDirections, cbusValue);

		cbusValue++;
		if (cbusValue > 0xf)
			cbusValue = 0;

		sleep(1);
	}

	return EXIT_SUCCESS;
}
