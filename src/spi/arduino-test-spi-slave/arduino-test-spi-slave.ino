/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of ft232-test.
 *
 * ft232-test is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ft232-test is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ft232-test.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This is a simple Arduino sketch for the Arduino Uno. It turns it into a SPI slave
 * that can be used with ft232-spi test mode (-T).
 *
 * Be sure to set the SPI clock frequency to a value the Arduino can handle. There are
 * some limits dictated by the MCU itself and some by the code below (i.e. the MCU has
 * to have enough time between SPI interrupts to run the ISR code). ATmega329P data sheet
 * says (in 18.5.2 SPSR – SPI Status Register):
 *
 *   When the SPI is configured as slave, the SPI is only guaranteed to work at f_{osc}/4 or lower.
 *
 * For the Arduino Uno, f_{osc} is 16 MHz, so theoretically 4 MHz should be doable. I
 * assume that is with polling, w/o interrupts. In reality, with the code below, I can 
 * use a clock frequency of 50 kHz and that works well. 100 kHz already breaks. So I run
 * ft232-spi like this:
 *
 * $ ft232-spi i:0x0403:0x6014 -c 50000 -T 100000
 *
 * The slave keeps replying to each byte transferred on SPI by a negation of the previous
 * byte. The very first byte (while the master is transferring the first byte) is a magic
 * value.
 *
 * This happens until SS is deasserted (set HIGH) and start again from the beginning when
 * SS is asserted.
 *
 * The Arduino Uno SPI pins are:
 * * 13: SCK
 * * 12: MISO
 * * 11: MOSI
 * * 10: SS
 *
 * Useful info about SPI on the ATmega328P can be found in the data sheet:
 * http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-7810-Automotive-Microcontrollers-ATmega328P_Datasheet.pdf
 */

constexpr uint8_t TEST_MAGIC = 0xab;

// SPI mode: polarity and phase
constexpr bool SPI_CPOL = 0;
constexpr bool SPI_CPHA = 0;

void setup()
{
  Serial.begin(115200);

  Serial.println();
  Serial.println("SPI test slave");
  Serial.print("config: CPOL=");
  Serial.print(SPI_CPOL);
  Serial.print(", CPHA=");
  Serial.print(SPI_CPHA);
  Serial.println();

  /*
   * Configure SPI:
   * * SPE - SPI enabled
   * * CPOL - set to 0 or 1 based on cpol constant
   * * CPHA - set to 0 or 1 based on cpha constant
   */
  SPCR = bit(SPE) | (SPI_CPOL * bit(CPOL)) | (SPI_CPHA * bit(CPHA));

  // Set MISO as output pin.
  pinMode(MISO, OUTPUT);
}

bool getSS()
{
  // directly reading PINB seems to be faster than digitalRead(SS)
  // (with digitalRead a test at 50 kHz fails, with direct PINB access it passes)
  return (PINB & bit(2)) == 0; // SS is PB2 on the Arduino Uno
}

void loop()
{
  // wait for SS becoming active
  while (!getSS())
    ;

  // while SS is active, keep talking over SPI
  uint32_t transferredBytes = 0;

  // prepare 1st byte to be sent
  SPDR = TEST_MAGIC;

  while (true)
  {
    bool ss = getSS();
    while (ss && (SPSR & bit(SPIF)) == 0)
      ss = getSS();

    if ((SPSR & bit(SPIF)) != 0)
    {
      // prepare a reply for next transfer: negated value of current byte - this is what the test expects
      SPDR = ~SPDR;

      transferredBytes++;
    }

    if (!ss)
      break;
  }
  
  Serial.print("SPI test transferred ");
  Serial.print(transferredBytes);
  Serial.println(" byte(s).");
}
