/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of ft232-test.
 *
 * ft232-test is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ft232-test is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ft232-test.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This example uses libftdi to do SPI communication in FT MPSSE mode.
 *
 * This is heavily inspired by https://github.com/UweBonnes/libftdispi but no
 * code was copied.
 */

#include <iostream>
#include <memory>
#include <stdexcept>
#include <sstream>
#include <string>
#include <limits>
#include <vector>
#include <algorithm>
#include <initializer_list>
#include <cstring>
#include <cstdlib>
#include <bitset>
#include <iomanip>
#include <cctype>
#include <optional>

#include <unistd.h>
#include <signal.h>

#include <ftdi.h>

#include "../common/FTDIError.h"
#include "../common/FTDIContext.h"
#include "../common/ParseFTDIInterface.h"
#include "../common/DumpBytes.h"
#include "../common/ParseBytes.h"

namespace
{

const char * PROGRAM_NAME = "ft232-spi";
bool verbose = false;

/*
 * libftdi does have #defines for MPSSE commands. Here we use:
 * * SET_BITS_LOW (0x80)
 * * GET_BITS_LOW (0x81)
 * * SET_BITS_HIGH (0x82)
 * * GET_BITS_HIGH (0x83)
 * * LOOPBACK_START (0x84)
 * * LOOPBACK_END (0x85)
 * * TCK_DIVISOR (0x86)
 * * EN_DIV_5 (0x8b)
 * * DIS_3_PHASE (0x8d)
 * * DIS_ADAPTIVE (0x97)
 *
 * These and other MPSSE commands are described in AN_108 - Command Processor for MPSSE
 * and MCU Host Bus Emulation Modes:
 * https://www.ftdichip.com/Support/Documents/AppNotes/AN_108_Command_Processor_for_MPSSE_and_MCU_Host_Bus_Emulation_Modes.pdf
 */

class FTDITransferControl
{
public:
	FTDITransferControl(FTDIContext & context, ftdi_transfer_control * tc):
		m_context(context),
		m_tc(tc)
	{}

	explicit operator bool() const
	{
		return m_tc != nullptr;
	}

	int finish()
	{
		const int r = finishImpl();
		if (r < 0)
			throw FTDIError(m_context, "ftdi_read_data_submit failed");

		return r;
	}

	~FTDITransferControl()
	{
		// don't throw from dtor
		finishImpl();
	}

private:
	int finishImpl()
	{
		int r = 0;
		if (m_tc)
		{
			r = ftdi_transfer_data_done(m_tc);
			m_tc = nullptr;
		}
		return r;
	}

	FTDIContext & m_context;
	ftdi_transfer_control * m_tc;
};

/**
 * A simple serializer for MPSSE commands.
 *
 * The doc (neither FTDI doc, nor libftdi doc) is particularly clear about
 * the size limit of a MPSSE command buffer. But it turns out any size is
 * OK. libftdi's ftdi_read_data/ftdi_write_data anyway transfers the data
 * in blocks of readbuffer_chunksize. So when larger block is used, liftdi
 * slices it up anyway. And the FTDI chip doesn't seem to mind a command
 * that is split into multiple chunks (so, multiple URBs?).
 *
 * But as the data is anyway sliced and diced, it also doesn't make sense to
 * try to keep it in 1 buffer. Just pick a reasonable size and use a vector
 * of vectors.
 */
class MPSSECommandSerializer
{
public:
	/**
	 * Create an instance of the MPSSE command serializer.
	 *
	 * @param[in] maxBufferSize Maximal size of a single buffer. When more data
	 * needs to be stored, a new buffer is created. There don't seem to be any
	 * real limitation on the size either libftdi or the FTDI chip, so any
	 * reasonable value will do.
	 */
	MPSSECommandSerializer(size_t maxBufferSize = 16*1024):
		m_maxBufferSize(maxBufferSize)
	{}

	// this sets ADBUS pins
	void setDataBitsLowByte(std::bitset<8> outDirections, std::bitset<8> outValues)
	{
		push({
			SET_BITS_LOW,
			static_cast<uint8_t>(outValues.to_ulong()),
			static_cast<uint8_t>(outDirections.to_ulong())
		});
	}

	// this reads ADBUS pins
	void readDataBitsLowByte()
	{
		push(GET_BITS_LOW);
	}

	// this sets ACBUS pins
	void setDataBitsHighByte(std::bitset<8> outDirections, std::bitset<8> outValues)
	{
		push({
			SET_BITS_HIGH,
			static_cast<uint8_t>(outValues.to_ulong()),
			static_cast<uint8_t>(outDirections.to_ulong())
		});
	}

	// this reads ACBUS pins
	void readDataBitsHighByte()
	{
		push(GET_BITS_HIGH);
	}

	void enableClockDivideBy5()
	{
		push(EN_DIV_5);
	}

	void setClockDivisor(uint16_t clockDivisor)
	{
		push({
			TCK_DIVISOR,
			static_cast<uint8_t>(clockDivisor & 0xff),
			static_cast<uint8_t>(clockDivisor >> 8)
		});
	}

	void disableThreePhaseDataClocking()
	{
		push(DIS_3_PHASE);
	}

	void disableAdaptiveClocking()
	{
		push(DIS_ADAPTIVE);
	}

	void setLoopback(bool enabled)
	{
		push(enabled ? LOOPBACK_START : LOOPBACK_END);
	}

	void writeBytes(uint8_t writeCommand /*TODO: better abstraction*/, const uint8_t * data, unsigned size)
	{
		serializeReadWriteCommandWithData(writeCommand, data, size);
	}

	void writeBits(uint8_t writeCommand /*TODO: better abstraction*/, uint8_t bits, unsigned size)
	{
		if (size == 0)
			return; // do nothing

		if (size > 8)
			throw std::runtime_error("writeBits can only write <= 8 bits");

		push({
			writeCommand,
			static_cast<uint8_t>(size - 1),
			bits
		});
	}

	void readBytes(uint8_t readCommand /*TODO: better abstraction*/, unsigned size)
	{
		while (size > 0)
		{
			const unsigned currentSize = std::min(MAX_COMMAND_PAYLOAD, size);
			push({
				readCommand,
				static_cast<uint8_t>((currentSize-1) & 0xff),
				static_cast<uint8_t>((currentSize-1) >> 8)
			});
			size -= currentSize;
		}
	}

	void readWriteBytes(uint8_t readWriteCommand /*TODO: better abstraction*/, const uint8_t * writeData, unsigned size)
	{
		serializeReadWriteCommandWithData(readWriteCommand, writeData, size);
	}

	void execCommand(FTDIContext & context)
	{
		if (verbose)
			std::cout << __func__ << ": writing " << std::dec << m_buffers.size() << " buffer(s):\n";

		for (auto & buffer : m_buffers)
		{
			if (verbose)
			{
				std::cout << "{ ";
				dumpBytes(std::cout, buffer);
				std::cout << "}" << std::endl;
			}

			if (ftdi_write_data(context, buffer.data(), buffer.size()) < 0)
				throw FTDIError(context, "ftdi_write_data failed");
		}

		m_buffers.clear();
	}

	void execCommandWithRead(FTDIContext & context, size_t size, std::vector<uint8_t> & data)
	{
		// initiate async read
		data.resize(size);

		FTDITransferControl tc(context, ftdi_read_data_submit(context, data.data(), data.size()));
		if (!tc)
		{
			data.clear();
			throw FTDIError(context, "ftdi_read_data_submit failed");
		}

		int r;
		try
		{
			execCommand(context);
			r = tc.finish();
		}
		catch (...)
		{
			data.clear();
			throw;
		}

		data.resize(r);
	}

private:
	void ensureBufferAvailable()
	{
		if (m_buffers.empty() || m_buffers.back().size() == m_maxBufferSize)
		{
			// allocate a new buffer
			m_buffers.emplace_back();
		}
	}

	void push(const std::initializer_list<uint8_t> & bytes)
	{
		const auto end = bytes.end();
		auto it = bytes.begin();

		while (it != end)
		{
			ensureBufferAvailable();
			m_buffers.back().push_back(*it);
			++it;
		}
	}

	void push(uint8_t b)
	{
		ensureBufferAvailable();
		m_buffers.back().push_back(b);
	}

	void push(const uint8_t * data, unsigned size)
	{
		const uint8_t * const end = data + size;
		while (data != end)
		{
			ensureBufferAvailable();
			const size_t bytesToCopy = std::min(m_maxBufferSize - m_buffers.back().size(), (size_t)(end - data));
			std::copy(data, data + bytesToCopy, std::back_inserter(m_buffers.back()));
			data += bytesToCopy;
		}
	}

	void serializeReadWriteCommandWithData(uint8_t command, const uint8_t * data, unsigned size)
	{
		while (size > 0)
		{
			const unsigned currentSize = std::min(MAX_COMMAND_PAYLOAD, size);
			push({
				command,
				static_cast<uint8_t>((currentSize-1) & 0xff),
				static_cast<uint8_t>((currentSize-1) >> 8)
			});
			push(data, currentSize);

			data += currentSize;
			size -= currentSize;
		}
	}

	// A single read/write command can do at most 0x10000 bytes.
	static constexpr unsigned MAX_COMMAND_PAYLOAD = 0x10000;

	size_t m_maxBufferSize;
	std::vector<std::vector<uint8_t>> m_buffers;
};

class FTDIMPSSE
{
public:
	FTDIMPSSE(const char * device, ftdi_interface iface):
		m_context(device, iface)
	{
		/*
		 * Enable MPSSE and configure for SPI. This is inspired by:
		 * * https://github.com/UweBonnes/libftdispi/blob/master/src/ftdispi.c
		 * * https://www.ftdichip.com/Support/Documents/AppNotes/AN_135_MPSSE_Basics.pdf
		 */

		/*
		 * Decrease the time the library is willing to wait for incomplete packet.
		 * This should decrease I/O latency as often the SPI device does not produce
		 * enough data to fill in full packet.
		 */
		if (ftdi_set_latency_timer(m_context, 2) < 0)
			throw FTDIError(m_context, "can't set latency");

		// libftdispi does this... but is it needed? how does flow control work with MPSSE?
		// https://www.ftdichip.com/Support/Documents/AppNotes/AN_135_MPSSE_Basics.pdf says
		// in 4.2 Configure FTDI Port For MPSSE Use: "Configure for RTS/CTS flow control to
		// ensure that the driver will not issue IN requests if the buffer is unable to accept
		// data."
		if (ftdi_setflowctrl(m_context, SIO_RTS_CTS_HS) < 0)
			throw FTDIError(m_context, "can't set flow control to RTS/CTS");

		// AN_135 says we should first do a RESET and then enable MPSSE
		if (ftdi_set_bitmode(m_context, 0, BITMODE_RESET) < 0)
			throw FTDIError(m_context, "can't reset bitmode");

		if (ftdi_set_bitmode(m_context, 0, BITMODE_MPSSE) < 0)
			throw FTDIError(m_context, "can't set bitmode to MPSSE");

		if (ftdi_usb_purge_buffers(m_context) < 0)
			throw FTDIError(m_context, "can't purge buffers");

		checkDevicePresent();
	}

	void enableLoopback(bool loopback)
	{
		m_serializer.setLoopback(loopback);
		m_serializer.execCommand(m_context);
	}

	void setClock(unsigned speed)
	{
		// AN_135 3.2 Clocks
		// TODO: add support for FTL232H 60MHz clock
		static constexpr unsigned CLOCK_FTL232R = 12000000;
		const unsigned divisor = (CLOCK_FTL232R/2) / speed - 1;
		if (divisor > 0xffff)
			throw std::runtime_error("unsupported speed TODO");

		m_serializer.setClockDivisor(divisor);
		m_serializer.execCommand(m_context);
	}

private:
	void checkDevicePresent()
	{
		/*
		 * Do what AN_135 recommends:
		 * Send a deliberately invalid command byte and check we get invalid-command reply.
		 * It's not so obvious what command is invalid command (what if a now-unused byte
		 * gets assigned a valid command in some later revision?), but let's use what AN_135
		 * example uses. This will hopefully stay unused.
		 */
		const uint8_t invalidCommand = 0xab;
		if (ftdi_write_data(m_context, &invalidCommand, sizeof(invalidCommand)) < 0)
			throw FTDIError(m_context, "ftdi_write_data failed");

		uint8_t reply[2];
		const int replySize = ftdi_read_data(m_context, reply, sizeof(reply));
		if (replySize < 0)
			throw FTDIError(m_context, "ftdi_read_data failed");

		const char ERROR_PREFIX[] = "Unexpected reply when checking device presence: ";
		if (replySize != 2)
		{
			std::stringstream s;
			s << ERROR_PREFIX << "got " << replySize << " bytes but expected 2";
			throw std::runtime_error(s.str());
		}

		if (reply[0] != 0xfa /* invalid command */ || reply[1] != invalidCommand)
		{
			std::stringstream s;
			s << ERROR_PREFIX << "unexpected reply";
			throw std::runtime_error(s.str());
		}
	}

protected:
	FTDIContext m_context;
	MPSSECommandSerializer m_serializer;
};

constexpr uint8_t MPSSE_LOW_BYTE_BIT_TCK = 1 << 0;
constexpr uint8_t MPSSE_LOW_BYTE_BIT_DO = 1 << 1;
constexpr uint8_t MPSSE_LOW_BYTE_BIT_CS = 1 << 3;

/*
 * Bits that need to be configured as output for SPI.
 * TCK/SK = SCLK
 * TDI/DO = MOSI
 * TMS/CS = SS
 * (TDO/DI is MISO and is configured as input)
 */
static constexpr uint8_t SPI_LOW_BYTE_DIRECTIONS =
		MPSSE_LOW_BYTE_BIT_TCK | MPSSE_LOW_BYTE_BIT_DO | MPSSE_LOW_BYTE_BIT_CS;

class FTDISPI: private FTDIMPSSE
{
public:
	FTDISPI(const char * device, ftdi_interface iface, unsigned clock,
		bool ssIdle, bool cpol, bool cpha, bool loopback):
		FTDIMPSSE(device, iface)
	{
		enableLoopback(loopback);
		setClock(clock);

		m_gpioLoDirections = SPI_LOW_BYTE_DIRECTIONS;
		m_gpioLoValue = ssIdle ? MPSSE_LOW_BYTE_BIT_CS : 0;

		m_writeCommand = MPSSE_DO_WRITE;
		m_readCommand = MPSSE_DO_READ;

		// https://github.com/UweBonnes/libftdispi/blob/master/src/ftdispi.c
		if (!cpol)
		{
			// clock is idle at 0

			if (!cpha)
			{
				// write on preceding falling edge, read on raising edge
				m_writeCommand |= MPSSE_WRITE_NEG;
			}
			else
			{
				// write on rising edge and read on falling edge
				m_readCommand |= MPSSE_READ_NEG;
			}
		}
		else
		{
			// clock is idle at 1
			m_gpioLoValue |= MPSSE_LOW_BYTE_BIT_TCK;

			if (!cpha)
			{
				// write on preceding rising edge (trailing edge of the previous cycle),
				// read on falling edge (leading edge of current cycle)
				m_readCommand |= MPSSE_READ_NEG;
			}
			else
			{
				// write on falling edge (leading edge of current cycle), read on rising
				// edge (trailing edge of current cycle)
				m_writeCommand |= MPSSE_WRITE_NEG;
			}
		}

		// set the Low bits to set the clock idle value and to set the SS to inactive
		m_serializer.setDataBitsLowByte(m_gpioLoDirections, m_gpioLoValue);
		m_serializer.execCommand(m_context);
	}

	void flushBuffers()
	{
		if (ftdi_usb_purge_buffers(m_context) < 0)
			throw FTDIError(m_context, "can't purge buffers");
	}

	/**
	 * Write bytes to SPI.
	 *
	 * 1) SS is asserted
	 * 2) data is sent (as 8 bit bytes, MSB-first) over SPI to the slave
	 * 4) SS is deasserted
	 */
	void writeBytes(const uint8_t * data, unsigned size)
	{
		// assert slave select
		flipSS();

		m_serializer.writeBytes(m_writeCommand, data, size);

		// deassert slave select
		flipSS();

		m_serializer.execCommand(m_context);
	}

	void writeBits(uint8_t bits, unsigned count)
	{
		if (count == 0)
			return; // do nothing

		if (count > 8)
			throw std::runtime_error("writeBits can only write <= 8 bits");

		// deassert slave select
		flipSS();

		m_serializer.writeBits(m_writeCommand | MPSSE_BITMODE, bits, count);

		// reset SS
		flipSS();

		m_serializer.execCommand(m_context);
	}

	void readBytes(unsigned size, std::vector<uint8_t> & data)
	{
		// assert slave select
		flipSS();

		m_serializer.readBytes(m_readCommand, size);

		// deassert slave select
		flipSS();

		m_serializer.execCommandWithRead(m_context, size, data);
	}

	void readBits()
	{
		// TODO
	}

	/**
	 * Full duplex writing & reading of data at the same time.
	 *
	 * 1) SS is asserted
	 * 2) size bytes is sent to the SPI slave at at the same time size bytes is read from the slave
	 * 3) SS is deasserted
	 */
	void readWriteBytes(std::vector<uint8_t> & readData, const uint8_t * writeData, unsigned size)
	{
		// assert slave select
		flipSS();

		// TODO: should there be m_readWriteCommand?
		const uint8_t readWriteCommand = m_readCommand | m_writeCommand;
		m_serializer.readWriteBytes(readWriteCommand, writeData, size);

		// deassert slave select
		flipSS();

		m_serializer.execCommandWithRead(m_context, size, readData);
	}

	void readWriteBits()
	{
		// TODO
	}

	/**
	 * Write data, then immediately read.
	 *
	 * This is not full-duplex reading and writing at the same time, instead:
	 *
	 * 1) SS is asserted
	 * 2) writeData is sent (as 8 bit bytes, MSB-first) over SPI to the slave
	 * 3) readSize bytes from the slave
	 * 4) SS is deasserted
	 */
	void writeAndReadBytes(const uint8_t * writeData, unsigned writeSize, std::vector<uint8_t> & readData, unsigned readSize)
	{
		// assert slave select
		flipSS();

		// a write followed by read (not at the same time)
		m_serializer.writeBytes(m_writeCommand, writeData, writeSize);
		m_serializer.readBytes(m_readCommand, readSize);

		// deassert slave select
		flipSS();

		m_serializer.execCommandWithRead(m_context, readSize, readData);
	}

	// the GPIOH (ACBUS 0-7) is free for any GPIO use
	void setGPIOHigh(std::bitset<8> outDirections, std::bitset<8> outValues)
	{
		m_serializer.setDataBitsHighByte(outDirections, outValues);
		m_serializer.execCommand(m_context);
	}

	// this allows setting only GPIOL4..7 as the low nibble is used for SPI communication
	void setGPIOLow(std::bitset<4> outDirections, std::bitset<4> outValues)
	{
		// only touch the high nibble
		m_gpioLoDirections = (m_gpioLoDirections & 0xf) | (outDirections.to_ulong() << 4);
		m_gpioLoValue = (m_gpioLoValue & 0xf) | (outValues.to_ulong() << 4);

		m_serializer.setDataBitsLowByte(m_gpioLoDirections, m_gpioLoValue);
		m_serializer.execCommand(m_context);
	}

private:
	/**
	 * Flip the value of the SPI SS line. (MPSSE output TMS/CS is used for SS.)
	 *
	 * The command is only added to the m_serializer. The caller is responsible
	 * for actually sending it to the chip.
	 */
	void flipSS()
	{
		m_gpioLoValue ^= MPSSE_LOW_BYTE_BIT_CS;
		m_serializer.setDataBitsLowByte(m_gpioLoDirections, m_gpioLoValue);
	}

	/**
	 * Directions of the GPIOL pins; 1 means output, 0 means input.
	 *
	 * The low 4 bytes are used for SPI (as SCLK (output), MOSI (output), MISO (input) and SS (output)).
	 * The remaining 4 bytes can be used for GPIO.
	 */
	uint8_t m_gpioLoDirections;

	/**
	 * The current value of the GPIOL output pins.
	 */
	uint8_t m_gpioLoValue;

	/**
	 * MPSSE command used for writing.
	 */
	uint8_t m_writeCommand;

	/**
	 * MPSSE command used for reading.
	 */
	uint8_t m_readCommand;
};

void printHelp()
{
		std::cout << "Usage: " << PROGRAM_NAME << " [-p cpol] [-P cpha] [-c clock] [-i interface] [-w bytes] [-r byteCount] [-W bytes] [-X bytes:byteCount] [-T byteCount] [-l] [-v] [-R] [-h] device_name\n"
				"\n"
				"A test program using libftdi to do SPI communication.\n"
				"\n"
				"Currently data is sent MSB-first.\n" // TODO: implement LSB first too
				"\n"
				"-p cpol\tSPI clock polarity (0 or 1; defaults to 0)\n"
				"-P cpha\tSPI clock phase (0 or 1; defaults to 0)\n"
				"-c clock\tSPI clock in Hz (defaults to 2000000, i.e. 2 MHz)\n"
				"-i interface\tRequests specific FTDI interface. Must be one of A, B, C or D. "
						"When not specified, ANY is passed to libftdi.\n"
				"-w bytes\tWrites given bytes. Use hex digits separate by comma for bytes, e.g. 'ff,0,a0'.\n"
				"-r byteCount\tReads given number of bytes and prints the result.\n"
				"-W bytes\tSame as -w but does a simultaneous read too. Read data is printed out.\n"
				"-X bytes:byteCount\tPerform a write of bytes immediately followed by a read of byteCount bytes.\n"
				"-l\tEnable loopback. This makes the chip behave as if TDI/DO and TDO/DI were connected.\n"
				"-v\tBe verbose. This logs the MPSSE commands that are sent to the FTDI chip.\n"
				"-T bytesCount\tRun a SPI test: Transfer given number of bytes and expect that for each byte "
				"(except for the last) that it sent, its negation comes back. At the end print 32bit checksum. "
				"This is intended to be run against a test client.\n"
				"-R\tRepeat the selected operation(s) forever.\n"
				"-h\tPrints this help.\n"
				"\n"
				"The device_name is passed to libftdi's ftdi_usb_open_string. You can use "
				"e.g. 'i:vendor:product' or 'i:vendor:product:index', for example 'i:0x0403:0x6014:1'\n";
}

bool parseBool(const char * s)
{
	auto i = std::stoul(s);
	if (i == 0)
		return false;
	else if (i == 1)
		return true;

	throw std::runtime_error("can't parse bool argument: please use '0' or '1'");
}

struct ICommand
{
	virtual void operator() (FTDISPI & spi) = 0;
	virtual ~ICommand() = default;
};

class WriteCommand: public ICommand
{
public:
	explicit WriteCommand(const char * arg)
	{
		parseBytes(optarg, m_writeData);
	}

	virtual void operator() (FTDISPI & spi) override
	{
		spi.writeBytes(m_writeData.data(), m_writeData.size());
		std::cout << "Wrote data" << std::endl;
	}

private:
	std::vector<uint8_t> m_writeData;
};

class ReadCommand: public ICommand
{
public:
	explicit ReadCommand(const char * arg):
		m_count(std::stoul(arg))
	{}

	virtual void operator() (FTDISPI & spi) override
	{
		m_readData.reserve(m_count);
		m_readData.resize(0);

		spi.readBytes(m_count, m_readData);
		std::cout << "Read: ";
		dumpBytes(std::cout, m_readData);
		std::cout << std::endl;
	}

private:
	unsigned m_count;
	std::vector<uint8_t> m_readData;
};

class FullDuplexWriteReadCommand: public ICommand
{
public:
	explicit FullDuplexWriteReadCommand(const char * arg)
	{
		parseBytes(optarg, m_writeData);
	}

	virtual void operator() (FTDISPI & spi) override
	{
		spi.readWriteBytes(m_readData, m_writeData.data(), m_writeData.size());
		std::cout << "Write & Read (full duplex), received: ";
		dumpBytes(std::cout, m_readData);
		std::cout << std::endl;
	}

private:
	std::vector<uint8_t> m_writeData;
	std::vector<uint8_t> m_readData;
};

class WriteReadCommand: public ICommand
{
public:
	explicit WriteReadCommand(const char * arg)
	{
		const char * separator = strchr(arg, ':');
		if (separator == nullptr)
			throw std::runtime_error("Use ':' to separate the bytes to be written from the number of bytes to read. E.g. -X 1,2,3:2");

		parseBytes(arg, separator, m_writeData);
		m_readCount = std::stoul(separator + 1);
	}

	virtual void operator() (FTDISPI & spi) override
	{
		spi.writeAndReadBytes(m_writeData.data(), m_writeData.size(), m_readData, m_readCount);
		std::cout << "Write & Read (sequentially), received: ";
		dumpBytes(std::cout, m_readData);
		std::cout << std::endl;
	}

private:
	std::vector<uint8_t> m_writeData;
	std::vector<uint8_t> m_readData;
	unsigned m_readCount;
};

class TestCommand: public ICommand
{
public:
	explicit TestCommand(const char * arg):
		m_count(std::stoul(arg))
	{
		if (m_count == 0)
			throw std::runtime_error("specify a positive number of bytes for SPI test (-T)");
	}

	virtual void operator() (FTDISPI & spi) override
	{
		std::vector<uint8_t> txBuffer;
		txBuffer.resize(m_count);

		for (size_t i = 0; i < txBuffer.size(); i++)
			txBuffer[i] = static_cast<uint8_t>(i);

		std::vector<uint8_t> rxBuffer;
		spi.readWriteBytes(rxBuffer, txBuffer.data(), txBuffer.size());

		// verify the result
		if (rxBuffer.size() != txBuffer.size())
		{
			std::stringstream s;
			s << "FTDI communication error: expected " << m_count << " bytes but received "
					<< rxBuffer.size() << "bytes";
			throw std::runtime_error(s.str());
		}

		// the 1st reply byte is not based on the data but the test wants a magic reply
		constexpr uint8_t TEST_MAGIC = 0xab;
		if (rxBuffer[0] != TEST_MAGIC)
		{
			std::stringstream s;
			s << "expecting reply to begin with magic byte "
					<< std::hex << std::showbase << (unsigned)TEST_MAGIC << " but got " << (unsigned)rxBuffer[0];
			throw std::runtime_error(s.str());
		}

		for (size_t i = 1; i < rxBuffer.size(); i++)
		{
			const uint8_t input = i-1;
			const uint8_t expectedOutput = ~input;

			if (rxBuffer[i] != expectedOutput)
			{
				std::stringstream s;
				s << "unexpected reply at offset " << i << ": expected " << std::hex << std::showbase
						<< (unsigned)expectedOutput << " but got " << (unsigned)rxBuffer[i];
				throw std::runtime_error(s.str());
			}
		}

		std::cout << "reply is correct" << std::endl;
	}

private:
	unsigned m_count;
};

volatile sig_atomic_t interrupted = 0;

void sigIntHandler(int)
{
	interrupted = 1;
}

void registerSigIntHandler()
{
	struct sigaction handler = {};

	handler.sa_handler = sigIntHandler;
	sigemptyset(&handler.sa_mask);

	sigaction(SIGINT, &handler, nullptr);
}

}

int main(int argc, char ** argv)
{
	if (argc > 0)
			PROGRAM_NAME = argv[0];

	const char * device = nullptr;
	ftdi_interface iface = INTERFACE_ANY;
	bool loopback = false;

	unsigned clock = 2000000 /*Hz*/;
	bool repeat = false;
	bool ssIdle = true;
	bool cpol = false;
	bool cpha = false;

	std::vector<std::unique_ptr<ICommand>> commands;

	int c;
	while ((c = getopt(argc, argv, "p:P:c:i:lw:r:W:X:T:vRh")) != -1)
	{
		switch (c)
		{
		case 'p':
			cpol = parseBool(optarg);
			break;

		case 'P':
			cpha = parseBool(optarg);
			break;

		case 'c':
			clock = std::stoul(optarg);
			break;

		case 'i':
			iface = parseFTDIInterface(optarg);
			break;

		case 'l':
			loopback = true;
			break;

		case 'w':
			commands.push_back(std::make_unique<WriteCommand>(optarg));
			break;

		case 'r':
			commands.push_back(std::make_unique<ReadCommand>(optarg));
			break;

		case 'W':
			commands.push_back(std::make_unique<FullDuplexWriteReadCommand>(optarg));
			break;

		case 'X':
			commands.push_back(std::make_unique<WriteReadCommand>(optarg));
			break;

		case 'T':
			commands.push_back(std::make_unique<TestCommand>(optarg));
			break;

		case 'v':
			verbose = true;
			break;

		case 'R':
			repeat = true;
			break;

		case 'h':
			printHelp();
			return EXIT_SUCCESS;

		default:
			std::cerr << "unknown option\n";
			return EXIT_FAILURE;
		}
	}

	if (optind == argc)
	{
			std::cerr << "Missing required argument: libftdi device name\n";
			return EXIT_FAILURE;
	}
	device = argv[optind];

	if (argc > optind + 1)
	{
		std::cerr << "Too many arguments.\n";
		return EXIT_FAILURE;
	}

	if (commands.empty())
	{
		std::cerr << "No operation requested. Specify at least one of -r, -w, -W or -T arguments.\n";
		return EXIT_FAILURE;
	}

	registerSigIntHandler();

	FTDISPI spi(device, iface, clock, ssIdle, cpol, cpha, loopback);

	std::cout << "Opened '" << device << "' in MPSSE mode" << std::endl;

	do
	{
		for (auto & command : commands)
		{
			if (interrupted)
				break;

			(*command)(spi);
		}

		if (repeat && !interrupted)
			sleep(1);
	} while (repeat && !interrupted);

	return EXIT_SUCCESS;
}
