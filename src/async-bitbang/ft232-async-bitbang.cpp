/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of ft232-test.
 *
 * ft232-test is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ft232-test is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ft232-test.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This example uses libftdi to do asynchronous bitbanging.
 *
 * This uses info from:
 * * https://www.ftdichip.com/Support/Documents/AppNotes/AN_232R-01_Bit_Bang_Mode_Available_For_FT232R_and_Ft245R.pdf
 * * libftdi bitbang examples
 */

#include <iostream>
#include <memory>
#include <stdexcept>
#include <sstream>
#include <string>
#include <limits>
#include <vector>
#include <algorithm>
#include <initializer_list>
#include <cstdlib>
#include <bitset>

#include <unistd.h>

#include <ftdi.h>

#include "../common/FTDIError.h"
#include "../common/FTDIContext.h"
#include "../common/ParseFTDIInterface.h"
#include "../common/DumpBytes.h"
#include "../common/ParseBytes.h"

namespace
{

const char * PROGRAM_NAME = "ft232-async-bitbang";

class FTDIAsyncBitbang
{
public:
	FTDIAsyncBitbang(const char * device, ftdi_interface iface, unsigned clock, std::bitset<8> outDirections):
		m_context(device, iface)
	{
		/*
		 * Enable asynchronous bitbang mode.
		 */

		// TODO: set chunk size?
		// ftdi_write_data_set_chunksize()

		// For MPSSE, AN_135 says we should first do a RESET and then enable MPSSE.
		// So do the same here: reset and then enable async bitbang
		if (ftdi_set_bitmode(m_context, 0, BITMODE_RESET) < 0)
			throw FTDIError(m_context, "can't reset bitmode");

		if (ftdi_set_bitmode(m_context, outDirections.to_ulong(), BITMODE_BITBANG) < 0)
			throw FTDIError(m_context, "can't set bitmode to asynchronous bitbang");

		if (ftdi_usb_purge_buffers(m_context) < 0)
			throw FTDIError(m_context, "can't purge buffers");

		/*
		 * As described in AN_232R-01, 2 Asynchronous Bit Bang Mode:
		 * The clock for the Asynchronous Bit Bang mode is actually 16 times the Baud rate.
		 * A value of 9600 Baud would transfer the data at (9600x16) = 153600 bytes per second,
		 * or 1 every 6.5 μS.
		 *
		 * ... but it seems this will in reality be more like 5.2 µS because the requested baud
		 * rate will be mapped to whatever the FDI chip can do, see:
		 * * the implementation of ftdi_set_baudrate
		 * * https://www.ftdichip.com/Support/Documents/AppNotes/AN_120_Aliasing_VCP_Baud_Rates.pdf
		 *
		 * ... so the precision pretty much depends on what is the actual value you need and
		 * what FTDI chip you use.
		 */

		const int s = clock/16;

		if (ftdi_set_baudrate(m_context, s) < 0)
			throw FTDIError(m_context, "can't set baud rate");
	}

	void write(const uint8_t * data, size_t size)
	{
		while (size > 0)
		{
			const int toWrite = std::min(size, static_cast<size_t>(std::numeric_limits<int>::max()));
			const int r = ftdi_write_data(m_context, data, toWrite);
			if (r < 0)
				throw FTDIError(m_context, "can'r write data to device");

			data += r;
			size -= r;
		}
	}

	void read(size_t size, std::vector<uint8_t> & data)
	{
		data.resize(size);
		uint8_t * p = data.data();

		while (size > 0)
		{
			const int toRead = std::min(size, static_cast<size_t>(std::numeric_limits<int>::max()));
			const int r = ftdi_read_data(m_context, p, toRead);
			if (r < 0)
			{
				data.clear();
				throw FTDIError(m_context, "can't read data from device");
			}

			p += r;
			size -= r;
		}

		data.resize(p - data.data());
	}

protected:
	FTDIContext m_context;
};

void printHelp()
{
	std::cout << "Usage: " << PROGRAM_NAME << " [-c clock] [-i interface] [-w bytes] [-r byteCount] [-h] device_name\n"
			"\n"
			"A test program using libftdi to do asynchronous bitbanging.\n"
			"\n"
			"-c clock\tClock in Hz (defaults to 100000, i.e. 100 kHz)\n"
			"-i interface\tRequests specific FTDI interface. Must be one of A, B, C or D. "
					"When not specified, ANY is passed to libftdi.\n"
			"-w bytes\tWrites given bytes. Use hex digits separate by comma for bytes, e.g. 'ff,0,a0'.\n"
			"-r byteCount\tReads given number of bytes and prints the result.\n"
			"-R\tRepeat the selected operation(s) forever.\n"
			"-h\tPrints this help.\n"
			"\n"
			"The device_name is passed to libftdi's ftdi_usb_open_string. You can use "
			"e.g. 'i:vendor:product' or 'i:vendor:product:index', for example 'i:0x0403:0x6014:1'\n";
}

}

int main(int argc, char ** argv)
{
	if (argc > 0)
		PROGRAM_NAME = argv[0];

	const char * device = nullptr;
	ftdi_interface iface = INTERFACE_ANY;
	std::vector<uint8_t> writeData;
	unsigned readBytes = 0;
	unsigned clock = 100000 /*Hz*/;
	bool repeat = false;

	int c;
	while ((c = getopt(argc, argv, "c:i:w:r:Rh")) != -1)
	{
		switch (c)
		{
		case 'c':
			clock = std::stoul(optarg);
			break;

		case 'i':
			iface = parseFTDIInterface(optarg);
			break;

		case 'w':
			parseBytes(optarg, writeData);
			break;

		case 'r':
			readBytes = std::stoul(optarg);
			break;

		case 'R':
			repeat = true;
			break;

		case 'h':
			printHelp();
			return EXIT_SUCCESS;

		default:
			std::cerr << "unknown option\n";
			return EXIT_FAILURE;
		}
	}

	if (optind == argc)
	{
		std::cerr << "Missing required argument: libftdi device name\n";
		return EXIT_FAILURE;
	}
	device = argv[optind];

	if (argc > optind + 1)
	{
		std::cerr << "Too many arguments.\n";
		return EXIT_FAILURE;
	}

	if (writeData.empty() && readBytes == 0)
	{
		std::cerr << "No operation requested. Specify one of -r or -w arguments.\n";
		return EXIT_FAILURE;
	}

	if (!writeData.empty() && readBytes != 0)
	{
		/*
		 * This could be supported if some bits were for reading and some for writing.
		 * Or one could change the direction of bits between operations but that doesn't
		 * sound too practical.
		 */
		std::cerr << "This example requires either read or a write op, not both.\n";
		return EXIT_FAILURE;
	}

	if (!writeData.empty())
	{
		std::cout << "Will write bytes: ";
		dumpBytes(std::cout, writeData);
		std::cout << '\n';
	}

	if (readBytes != 0)
		std::cout << "Will read " << readBytes << " bytes.\n";

	std::cout.flush();

	std::vector<uint8_t> readBuffer;
	FTDIAsyncBitbang asyncBitbang(device, iface, clock,
			readBytes != 0 ? 0 /*all input*/ : 0xff /*all output*/);

	do
	{
		if (writeData.empty())
		{
			std::cout << "Writing..." << std::endl;
			asyncBitbang.write(writeData.data(), writeData.size());
		}

		if (readBytes != 0)
		{
			asyncBitbang.read(readBytes, readBuffer);
			std::cout << "Read " << readBuffer.size() << " byte(s): ";
			dumpBytes(std::cout, readBuffer);
			std::cout << std::endl;
		}

		if (repeat)
			sleep(1);
	}
	while (repeat);

	return EXIT_SUCCESS;
}
